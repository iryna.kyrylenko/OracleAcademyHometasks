package ua.org.oa.iryna.kyrylenko.task4_2.part2;

import java.util.Iterator;

public class Demo {

	public static void main(String[] args) {
		
		MyDeque<Integer> deque1 = new MyDequeImpl<>();
		deque1.addLast(100);
		deque1.addLast(99);
		deque1.addLast(18);
		deque1.addLast(46);
		deque1.addLast(3);
		deque1.addLast(99);

		Iterator<Integer> itr = deque1.iterator();

		System.out.println(deque1.toString());

		while (itr.hasNext()) { // ���� while ������������� �������� ��
								// ���������� ������� MyDequeImpl, � �����
								// ������ ������ remove();
			System.out.println(itr.next());
			itr.remove();
		}
		System.out.println(deque1.toString() + "\n");

		MyDeque<String> deque2 = new MyDequeImpl<>();
		deque2.addFirst("mine");
		deque2.addFirst("is");
		deque2.addFirst("world");
		deque2.addFirst("The");
		Iterator<String> itr2 = deque2.iterator();

		System.out.println(deque2.toString());

		for (String str : deque2) { // ���� foreach �������������, ��� �������
									// DequeImpl �������� ����������� ��������
			System.out.println(str);
		}

	}
}
