package ua.org.oa.iryna.kyrylenko.task4_1;

public class Computer implements Comparable<Computer> {
	private String model;
	private Integer hdd;
	private Integer ram;
public Computer(String model, Integer hdd, Integer ram) {
setModel(model);
setHdd(hdd);
setRam(ram);
}
	@Override
	public int compareTo(Computer comp) {
		return ram.compareTo(comp.getRam());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hdd == null) ? 0 : hdd.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((ram == null) ? 0 : ram.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Computer other = (Computer) obj;
		if (hdd == null) {
			if (other.hdd != null)
				return false;
		} else if (!hdd.equals(other.hdd))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (ram == null) {
			if (other.ram != null)
				return false;
		} else if (!ram.equals(other.ram))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Computer [model=" + model + ", hdd=" + hdd + ", ram=" + ram + "]";
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getHdd() {
		return hdd;
	}

	public void setHdd(Integer hdd) {
		this.hdd = hdd;
	}

	public Integer getRam() {
		return ram;
	}

	public void setRam(Integer ram) {
		this.ram = ram;
	}
}
