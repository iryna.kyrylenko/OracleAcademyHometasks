package ua.org.oa.iryna.kyrylenko.task4_1;

public class Car {
private String model;
private String colour;
private int year;
public Car(String model, String colour, int year) {
	setColour(colour);
	setModel(model);
	setYear(year);
}
public String getModel() {
	return model;
}
public void setModel(String model) {
	this.model = model;
}
public String getColour() {
	return colour;
}
public void setColour(String colour) {
	this.colour = colour;
}
public int getYear() {
	return year;
}
public void setYear(int year) {
	this.year = year;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((colour == null) ? 0 : colour.hashCode());
	result = prime * result + ((model == null) ? 0 : model.hashCode());
	result = prime * result + year;
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Car other = (Car) obj;
	if (colour == null) {
		if (other.colour != null)
			return false;
	} else if (!colour.equals(other.colour))
		return false;
	if (model == null) {
		if (other.model != null)
			return false;
	} else if (!model.equals(other.model))
		return false;
	if (year != other.year)
		return false;
	return true;
}
@Override
public String toString() {
	return "Car [model=" + model + ", colour=" + colour + ", year=" + year + "]";
}

}
