package ua.org.oa.iryna.kyrylenko.task4_2;

import java.util.*;

public class Demo {

	public static void main(String[] args) {

		MyDeque<Integer> dek = new MyDequeImpl<>();
		dek.addLast(1);
		dek.addLast(5);
		dek.removeLast(); //������� 5
		System.out.println(dek.toString());// ����� �������� 1
		dek.removeFirst(); //������� 1
		dek.addFirst(7);//��������� 7
		System.out.println(dek.toString()); // ��� ��� ������� �� ������ �������� - 7
		
		dek.clear();//�������� ������
		System.out.println(dek.toString());//������ ������ ���
		
		dek.addFirst(15);//��������� � ������ 15
		dek.addFirst(19);//��������� � ������ 19
		dek.addLast(25);//��������� � ����� 25
		System.out.println(dek.getFirst());//������ 19
		System.out.println(dek.getLast());//������25
		System.out.println(dek.size());//3
		System.out.println(dek.contains(25));// true
		System.out.println(dek.contains(3));//false
		System.out.println(dek.toString());//19, 15, 25
		
		System.out.println(Arrays.toString(dek.toArray()));//������� ������ ���������

		MyDeque<Integer> dek_all = new MyDequeImpl<>();
		dek_all.addFirst(19);
		dek_all.addFirst(7);
		dek_all.addFirst(25);
		dek_all.addFirst(15);//��������� dek_all ����������� ��������� � dek
		System.out.println(dek.containsAll(dek_all)); //true
		
		MyDeque<Integer> dek_wrong = new MyDequeImpl<>();
		dek_wrong.addFirst(19);
		dek_wrong.addFirst(7);
		dek_wrong.addFirst(25);
		dek_wrong.addFirst(1345);//�������� ������ Integer
		System.out.println(dek.containsAll(dek_wrong));//false
		
		
	}
}
