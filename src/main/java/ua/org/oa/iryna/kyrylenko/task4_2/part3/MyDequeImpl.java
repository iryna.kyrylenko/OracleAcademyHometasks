package ua.org.oa.iryna.kyrylenko.task4_2.part3;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class MyDequeImpl<E> implements MyDeque<E> {
	private int size;
	private Node<E> first;
	private Node<E> last;
	private Object[] array;

	/**
	 * Creates a listIterator
	 */
	@Override
	public ListIterator<E> listIterator() {
		return new ListIteratorImpl();
	}

	public MyDequeImpl() {
	}

	/**
	 * Creates an iterator
	 */
	@Override
	public Iterator<E> iterator() {
		return new IteratorImpl();
	}

	/**
	 * Adds an element in front of MyDequeImpl
	 */
	@Override
	public void addFirst(E e) {
		final Node<E> constNext = first;
		final Node<E> temp = new Node<E>(null, e, constNext);
		first = temp;
		if (constNext == null) {
			last = temp;
		} else {
			constNext.prev = temp;
		}
		size++;
	}

	/**
	 * Adds an element in back of MyDequeImpl
	 */
	@Override
	public void addLast(E e) {
		final Node<E> constPrev = last;
		final Node<E> temp = new Node<E>(constPrev, e, null);
		last = temp;
		if (constPrev == null) {
			first = temp;
		} else {
			constPrev.next = temp;
		}
		size++;
	}

	/**
	 * Removes an element in front of MyDequeImpl
	 */
	@Override
	public E removeFirst() {
		Node<E> temp = first;
		if (size == 1) {
			first = null;
		} else {
			first = first.next;
			first.prev = null;
		}

		size--;
		return temp.element;
	}

	/**
	 * Removes an element in back of MyDequeImpl
	 */
	@Override
	public E removeLast() {
		Node<E> temp = last;
		if (size == 1) {
			last = null;
		} else {
			last = last.prev;
			last.next = null;
		}

		size--;
		return temp.element;
	}

	/**
	 * Returns the first element of MyDequeImpl
	 */
	@Override
	public E getFirst() {
		return first.element;
	}

	/**
	 * Returns the last element of MyDequeImpl
	 */
	@Override
	public E getLast() {
		return last.element;
	}

	/**
	 * Returns true if MyDequeImpl contains entering Object
	 */
	@Override
	public boolean contains(Object o) {
		int i = 0;
		for (Node<E> n = first; i < size; n = n.next) {
			if (n.element.equals(o)) {
				return true;
			}
			i++;
		}
		return false;
	}

	/**
	 * Removes all elements from MyDequeImpl
	 */
	@Override
	public void clear() {
		int temp = this.size;
		for (int i = 0; i < temp; i++) {
			removeFirst();
		}
	}

	/**
	 * Creates an array and fills it with all elements of MyDequeImpl
	 */
	@Override
	public Object[] toArray() {
		this.array = new Object[size];
		int i = 0;
		for (Node<E> n = first; i < size; n = n.next) {
			array[i] = n.element;
			i++;
		}
		return array;
	}

	/**
	 * Returns current size of MyDequeImpl
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * Returns true if current MyDequeImpl contains all elements of entering
	 * deque
	 */
	@Override
	public boolean containsAll(MyDeque<? extends E> deque) {
		int i = 0;
		for (Node<E> n = first; i < this.size; n = n.next) {
			if (deque.contains(n.element) != true) {
				return false;
			}
			i++;
		}
		return true;
	}

	@Override
	public String toString() {
		toArray();
		return "This MyDequeImpl contains: " + Arrays.toString(array);
	}

	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> prev;

		Node(Node<E> prev, E element, Node<E> next) {
			this.prev = prev;
			this.element = element;
			this.next = next;

		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

	private class IteratorImpl implements Iterator<E> {
		
		private Node<E> current;
		private boolean removed;

		/**
		 * Checks is there an element next to iterator or not
		 */
		@Override
		public boolean hasNext() {
			if (current == null) {
				return first != null;
			} else {
				return current.next != null;
			}
		}

		/**
		 * Shifts an iterator to the right Returns an element on current
		 * iterator's position
		 */
		@Override
		public E next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			if (current == null) {
				current = first;
			} else {
				current = current.next;
			}
			E result = current.element;
			removed = false;
			return result;
		}

		/**
		 * Removes an element on current iterator's position
		 */
		public void remove() {
			if (current == null || removed) {
				throw new IllegalStateException();
			}
			if (current.prev == null) {
				first = current.next;
				current.next.prev = null;
			} else if (current.next == null) {
				last = current.prev;
				current.prev.next = null;
			} else {
				current.prev.next = current.next;
				current.next.prev = current.prev;
			}
			removed = true;
			size--;
		}
	}

	private class ListIteratorImpl extends IteratorImpl implements ListIterator<E> {
		
		/**
		 * Checks is there an element previous to iterator or not
		 */
		@Override
		public boolean hasPrevious() {
			return super.current != null ;
		}

		/**
		 * Shifts an iterator to the left Returns an element on current iterator
		 * position
		 */
		@Override
		public E previous() {
			if (!hasPrevious()) {
				throw new NoSuchElementException();
			}
			E result = super.current.element;
			super.current = super.current.prev;
			return result;
		}

		/**
		 * Sets a new value for the Node element
		 */
		@Override
		public void set(E e) {
			super.current.element = e;
		}

	}
}
