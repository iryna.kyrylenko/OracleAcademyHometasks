package ua.org.oa.iryna.kyrylenko.task4_2.part3;

public interface ListIterable<E> {
	ListIterator<E> listIterator();
}
