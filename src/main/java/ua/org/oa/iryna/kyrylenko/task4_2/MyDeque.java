package ua.org.oa.iryna.kyrylenko.task4_2;

public interface MyDeque<E> {

	void addFirst(E e);

	void addLast(E e);

	E removeFirst();

	E removeLast();

	E getFirst();

	E getLast();

	boolean contains(Object o);

	void clear();

	Object[] toArray();

	int size();

	boolean containsAll(MyDeque<? extends E> deque);
}
