package ua.org.oa.iryna.kyrylenko.task4_2.part3;

import java.util.Iterator;

public interface ListIterator<E> extends Iterator<E> {

	// ���������, ���� �� ���������� ������� ��� ������� ������� previous

	boolean hasPrevious();

	// ���������� ���������� �������

	E previous();

	// �������� �������, ������� �� ���������� ���� ��� ��������� next/previous
	// �� ������ �������

	void set(E e);

	// ������� �������, ������� �� ���������� ���� ��� ��������� next/previous

	void remove();

}
