package ua.org.oa.iryna.kyrylenko.task4_2;

import java.util.Arrays;
import java.util.Collections;

public class MyDequeImpl<E> implements MyDeque<E> {
	private int size;
	private Node<E> first;
	private Node<E> last;
	private Object[] array;

	public MyDequeImpl() {
	}

	/**
	 * Adds an element in front of MyDequeImpl
	 */
	@Override
	public void addFirst(E e) {
		final Node<E> constNext = first;
		final Node<E> temp = new Node<E>(null, e, constNext);
		first = temp;
		if (constNext == null) {
			last = temp;
		} else {
			constNext.prev = temp;
		}
		size++;
	}

	/**
	 * Adds an element in back of MyDequeImpl
	 */
	@Override
	public void addLast(E e) {
		final Node<E> constPrev = last;
		final Node<E> temp = new Node<E>(constPrev, e, null);
		last = temp;
		if (constPrev == null) {
			first = temp;
		} else {
			constPrev.next = temp;
		}
		size++;
	}

	/**
	 * Removes an element in front of MyDequeImpl
	 */
	@Override
	public E removeFirst() {
		Node<E> temp = first;
		if (size == 1) {
			first = null;
		} else {
			first = first.next;
			first.prev = null;
		}

		size--;
		return temp.element;
	}

	/**
	 * Removes an element in back of MyDequeImpl
	 */
	@Override
	public E removeLast() {
		Node<E> temp = last;
		if (size == 1) {
			last = null;
		} else {
			last = last.prev;
			last.next = null;
		}

		size--;
		return temp.element;
	}

	/**
	 * Returns the first element of MyDequeImpl
	 */
	@Override
	public E getFirst() {
		return first.element;
	}

	/**
	 * Returns the last element of MyDequeImpl
	 */
	@Override
	public E getLast() {
		return last.element;
	}

	/**
	 * Returns true if MyDequeImpl contains entering Object
	 */
	@Override
	public boolean contains(Object o) {
		int i = 0;
		for (Node<E> n = first; i < size; n = n.next) {
			if (n.element.equals(o)) {
				return true;
			}
			i++;
		}
		return false;
	}

	/**
	 * Removes all elements from MyDequeImpl
	 */
	@Override
	public void clear() {
		int temp = this.size;
		for (int i = 0; i < temp; i++) {
			removeFirst();
		}
	}

	/**
	 * Creates an array and fills it with all elements of MyDequeImpl
	 */
	@Override
	public Object[] toArray() {
		this.array = new Object[size];
		int i = 0;
		for (Node<E> n = first; i < size; n = n.next) {
			array[i] = n.element;
			i++;
		}
		return array;
	}

	/**
	 * Returns current size of MyDequeImpl
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * Returns true if current MyDequeImpl contains all elements of entering
	 * deque
	 */
	@Override
	public boolean containsAll(MyDeque<? extends E> deque) {
		int i = 0;
		for (Node<E> n = first; i < this.size; n = n.next) {
			if (deque.contains(n.element) != true) {
				return false;
			}
			i++;
		}
		return true;
	}

	@Override
	public String toString() {
		toArray();
		return "MyDequeImpl contains: " + Arrays.toString(array);
	}

	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> prev;

		Node(Node<E> prev, E element, Node<E> next) {
			this.prev = prev;
			this.element = element;
			this.next = next;

		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

}
