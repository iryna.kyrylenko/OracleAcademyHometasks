package ua.org.oa.iryna.kyrylenko.task4_2.part3;

public class Demo {

	public static void main(String[] args) {
		MyDeque<Integer> deque1 = new MyDequeImpl<>();

		deque1.addLast(4);
		deque1.addLast(17);
		deque1.addLast(25);
		deque1.addLast(30);
		deque1.addLast(34);

		ListIterator<Integer> itr1 = deque1.listIterator();

		while (itr1.hasNext()) {
			System.out.println("This is loop for next() :"+itr1.next());
		}
		while (itr1.hasPrevious()){
			System.out.println("This is loop for next() :"+itr1.previous());
		}
		for (Integer integer : deque1) {
			System.out.println(deque1);
		}
		
	
	}
}
