package ua.org.oracle_academy.hometask5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class Translator {

	private static final String DICTIONARIES_DIR = "dictionaries";
	private static final String TRANSLATION_REGEXP = "\\p{L}+=\\p{L}+";
	private static final String EQUALS_CHARACTER = "=";
	private static final String TXT_FILE_EXTENSION = ".txt";
	private static final String UTF8_ENCODING = "UTF-8";
	private static final String EXIT_COMMAND = "exit";
	
	public static void main(String[] args) throws UnsupportedEncodingException, IOException {
		Translator translator = new Translator();
		Map<String, Map<String, String>> dictionaries = readDictionaries(DICTIONARIES_DIR);
		if (dictionaries.size() == 0) {
			throw new IllegalStateException("No dictionaries were initialized!");
		}
		System.out.println("Welcome to translator!");
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, UTF8_ENCODING))) {
			String dictionaryName = selectDictionary(reader, dictionaries.keySet().toArray());
			System.out.println("Dictionary '" + dictionaryName + "' selected.");
			Map<String, String> dictionary = dictionaries.get(dictionaryName);
			while(true) {
				System.out.println("Please type word to get translation:");
				try {
					String input = reader.readLine().trim();
					isExitTyped(input);
					System.out.println(input + " -> " + translator.translate(input, dictionary));
				} catch (NoSuchElementException e) {
					System.out.println(e.getMessage());
					System.out.println("Tip: you can type 'exit' to exit program");
				} catch (IOException e) {
					System.out.println("Invalid input! Please try again.");
					System.out.println("Tip: you can type 'exit' to exit program");
				}
			}
		}
	}
	
	public String translate(String string, Map<String, String> dictionary) {
		if (dictionary.containsKey(string)) {
			return dictionary.get(string);
		} else {
			throw new NoSuchElementException("This dictionary does not contains such word!");
		}
	}
	
	public static Map<String, Map<String, String>> readDictionaries(String directory) {
		Map<String, Map<String, String>> dictionaries = new HashMap<String, Map<String, String>>();
		try {
			for (File file : new File(directory).listFiles()) {
				if (isDictionary(file)) {
					dictionaries.put(getDictionaryName(file.getName()), readDictionary(file));
				}
			}
		} catch (IOException e) {
			throw new RuntimeException("Error during dictionaries initialization!", e);
		}
		return dictionaries;
	}
	
	public static Map<String, String> readDictionary(File file) throws IOException {
		Map<String, String> dictionary = new HashMap<String, String>();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), UTF8_ENCODING))) {
			for (String line; (line = reader.readLine()) != null;) {
				if (isValidTranslation(line)) {
					dictionary.put(getKey(line), getValue(line));
				}
			}
			return dictionary;
		}
	}
	
	private static boolean isDictionary(File file) {
		return file.isFile() && file.getName().endsWith(TXT_FILE_EXTENSION);
	}
	
	private static String getKey(String string) {
		return string.split(EQUALS_CHARACTER)[0];
	}
	
	private static String getValue(String string) {
		return string.split(EQUALS_CHARACTER)[1];
	}
	
	private static boolean isValidTranslation(String string) {
		return string.matches(TRANSLATION_REGEXP);
	}
	
	private static String getDictionaryName(String string) {
		return string.substring(0, string.length() - TXT_FILE_EXTENSION.length());
	}
	
	private static void printDictionaries(Object[] dictionaries) {
		int i = 0;
		for (Object dictionary : dictionaries) {
			System.out.println(++i + " : " + dictionary);
		}
	}
	
	private static String selectDictionary(BufferedReader reader, Object[] dictoinaries) {
		while (true) {
			try {
				System.out.println("Please select dictionary:");
				printDictionaries(dictoinaries);
				System.out.println("Tip: you can type 'exit' to exit program");
				String input = reader.readLine().trim();
				isExitTyped(input);
				if (dictoinaries[Integer.parseInt(input) - 1] != null) {
					return (String) dictoinaries[Integer.parseInt(input) - 1];
				}
			} catch (ArrayIndexOutOfBoundsException | IOException | NumberFormatException e) {
				System.out.println("Invalid input! Please try again.");
			}
		}
	}
	
	private static void isExitTyped(String string) {
		if (string.equalsIgnoreCase(EXIT_COMMAND)) {
			System.exit(0);
		}
	}
	
}
