package ua.org.oracle_academy.practice1;

public class ArrayProd {
	public static int multiplyArray(int[] maz) {
		int result = 1;
		for (int i : maz) {
			result *= i;
		}
		return result;
	}
}
