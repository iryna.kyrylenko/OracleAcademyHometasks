package ua.org.oracle_academy.practice1;

public class Group {

	private int course;
	private String faculty;

	public Group(String faculty, int course) {
		setFaculty(faculty);
		setCourse(course);

	}

	public int getCourse() {
		return course;
	}

	public void setCourse(int course) {
		if (course < 1 || course > 5) {
			throw new IllegalArgumentException("������������ ��������, �������� ���� �� 1 �� 5");
		}
		this.course = course;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

}
