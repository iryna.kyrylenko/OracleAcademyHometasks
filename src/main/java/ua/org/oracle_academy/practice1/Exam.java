package ua.org.oracle_academy.practice1;

import java.util.Set;

public class Exam {

	private int mark;
	private int year;
	private int semester;
	private String subject;

	public Exam() {
	}

	public Exam(int mark, int year, int semester, String subject) {
		setMark(mark);
		setYear(year);
		setSemester(semester);
		setSubject(subject);

	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "Exam [mark=" + mark + ", year=" + year + ", semester=" + semester + ", subject=" + subject + "]";
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		if (mark < 1 || mark > 12) {
			throw new IllegalArgumentException("������������ ��������, �������� ������ �� 1 �� 12");
		}
		this.mark = mark;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (year < 2010 || year > 2017) {
			throw new IllegalArgumentException("������������ ��������, �������� ������ �� 2010 �� 2017");
		}
		this.year = year;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mark;
		result = prime * result + semester;
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exam other = (Exam) obj;
		if (mark != other.mark)
			return false;
		if (semester != other.semester)
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		if (semester == 1 || semester == 2) {
			this.semester = semester;
		} else {
			throw new IllegalArgumentException("������������ ��������, �������� 1 ��� 2 �������");
		}
	}

}
