package ua.org.oracle_academy.practice1;

public class ArraySum {

	private int[] nums;

	public ArraySum(int[] nums) {
		this.nums = nums;

	}

	public int sum() {
		return sum(this.nums);
	}

	public static int sum(int[] nums) {
		int result = 0;
		for (int i = 0; i < nums.length; i++) {
			result = result + nums[i];
		}

		return result;
	}
}
