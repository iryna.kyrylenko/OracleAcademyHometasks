package ua.org.oracle_academy.practice1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Student {

	private String firstName;
	private String lastName;
	private Group group;
	private List<Exam> exams;

	public Student(String firstName, String lastName, Group group) {
		exams = new ArrayList<Exam>();
	}

	public void addMark(Exam exam) {
		this.exams.add(exam);
	}

	public void deleteMark(Exam exam) {
		this.exams.remove(exam);
	}

	public int returnTheHighestMark(String subject) {
		for (Exam exam : exams) {

		}

		return 0;
	}

	public String printList() {
		return String.valueOf(exams);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
