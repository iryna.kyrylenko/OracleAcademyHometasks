package ua.org.oracle_academy.practice6;

public class MyRunnable implements Runnable {
	private String runName;
	
public String getRunName() {
		return runName;
	}
	public void setRunName(String runName) {
		this.runName = runName;
	}
public MyRunnable(String runName) {
	this.runName = runName;
}
	@Override
	public void run() {
		int i = 0;
		Thread t = new Thread(this);
		t.setName(this.runName);
		while (i < 100000) {
			System.out.println(t.getName());
			i++;
		}

	}

}
