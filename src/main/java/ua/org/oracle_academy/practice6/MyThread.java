package ua.org.oracle_academy.practice6;

public class MyThread extends Thread {
	private String thrName;
	
public MyThread(String thrName) {
	this.thrName = thrName;
}
	@Override
	public void run() {
		int i = 0;
		while (i < 100000) {
			System.out.println(this.getName());
			i++;
		}
	}
	public String getThrName() {
		return thrName;
	}
	public void setThrName(String thrName) {
		this.thrName = thrName;
	}
}
