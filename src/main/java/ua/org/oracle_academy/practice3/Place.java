package ua.org.oracle_academy.practice3;

public class Place {
	private Row row;
	private RowPlace rowPlace;

	public Place(Row row, RowPlace rowPlace) {
		this.row = row;
		this.rowPlace = rowPlace;
	}

	private enum Row {
		FIRST(1), SECOND(2), THIRD(3), FOURTH(4), FIFTH(5), SIXTH(6), SEVENTH(7), EIGTHS(8), NINTH(9);
		private final int mask;

		private Row(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}
	}

	private enum RowPlace {
		FIRST(1), SECOND(2), THIRD(3), FOURTH(4), FIFTH(5), SIXTH(6), SEVENTH(7), EIGTHS(8), NINTH(9);
		private final int mask;

		private RowPlace(int mask) {
			this.mask = mask;
		}

		public int getMask() {
			return mask;
		}
	}
}
