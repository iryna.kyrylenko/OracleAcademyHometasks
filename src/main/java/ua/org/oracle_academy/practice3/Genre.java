package ua.org.oracle_academy.practice3;

public enum Genre {
	COMEDY, DRAMA, HORROR, THRILLER, VINTAGE, ARTHOUSE, FANTASTIC, HISTORY;
}
