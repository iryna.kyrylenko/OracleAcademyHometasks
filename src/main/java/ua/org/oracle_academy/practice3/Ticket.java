package ua.org.oracle_academy.practice3;

public class Ticket {
private User id;
private Seance seance;
private Movie movie;
private Place place;

public Ticket(User id,Seance seance,Movie movie,Place place) {
	this.id = id;
	this.seance = seance;
	this.movie = movie;
	this.place = place;
}

public User getId() {
	return id;
}

public void setId(User id) {
	this.id = id;
}

public Seance getSeance() {
	return seance;
}

public void setSeance(Seance seance) {
	this.seance = seance;
}

public Movie getMovie() {
	return movie;
}

public void setMovie(Movie movie) {
	this.movie = movie;
}

public Place getPlace() {
	return place;
}

public void setPlace(Place place) {
	this.place = place;
}

@Override
public String toString() {
	return "Ticket [id=" + id + ", seance=" + seance + ", movie=" + movie + ", place=" + place + "]";
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((movie == null) ? 0 : movie.hashCode());
	result = prime * result + ((place == null) ? 0 : place.hashCode());
	result = prime * result + ((seance == null) ? 0 : seance.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Ticket other = (Ticket) obj;
	if (id == null) {
		if (other.id != null)
			return false;
	} else if (!id.equals(other.id))
		return false;
	if (movie == null) {
		if (other.movie != null)
			return false;
	} else if (!movie.equals(other.movie))
		return false;
	if (place == null) {
		if (other.place != null)
			return false;
	} else if (!place.equals(other.place))
		return false;
	if (seance == null) {
		if (other.seance != null)
			return false;
	} else if (!seance.equals(other.seance))
		return false;
	return true;
}

}
