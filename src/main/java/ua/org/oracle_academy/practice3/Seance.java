package ua.org.oracle_academy.practice3;

import java.util.Date;

public class Seance {
	private long id;
	private Date time;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Seance [id=" + id + ", time=" + time + "]";
	}
}
