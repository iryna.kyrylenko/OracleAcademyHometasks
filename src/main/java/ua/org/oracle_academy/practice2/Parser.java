package ua.org.oracle_academy.practice2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

public class Parser {
	
	public static String readFromFile(String path, String charSet) {
		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), charSet))){
			String s = null;
			while ((s = br.readLine())!= null){
                sb.append(s).append("\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
				
		return sb.toString();
	}
	
	public static List <Rank> parseRank(String path, String charSet, String regex){
		List <Rank> rankList = new ArrayList<>();
		String html = readFromFile(path, charSet);
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(html);
		Rank rank = null;
		while(matcher.find()){
			rank = new Rank();
			rank.setRank(matcher.group("rank"));  
			rank.setFemaleName(matcher.group("femaleName"));
			rank.setMaleName(matcher.group("maleName"));
			rankList.add(rank);
		}
		return rankList;
	}
	public static List <Rank> parseNotebook(String path, String charSet, String regex){
		List <Notebook> notebookList = new ArrayList<>();
		
		return null;
	}
public static class Rank{
	private String rank;
	private String femaleName;
	private String maleName;
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getFemaleName() {
		return femaleName;
	}
	public void setFemaleName(String femaleName) {
		this.femaleName = femaleName;
	}
	public String getMaleName() {
		return maleName;
	}
	public void setMaleName(String maleName) {
		this.maleName = maleName;
	}
	@Override
	public String toString() {
		return "Rank [rank=" + rank + ", femaleName=" + femaleName
				+ ", maleName=" + maleName + "]";
	}
}
	
public static class Notebook{
	private String name;
	private String description;
	private double price;
}
}