package ua.org.oracle_academy.practice4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class StudentUtils {

	public static Map<String, Student> createMapFromList(List<Student> students) {
		Map<String, Student> resultMap = new HashMap<>();
		for (Student student : students) {
			resultMap.put(student.getKeyname(), student);
		}
		return resultMap;
	}

	public static void printStudents(List<Student> list, int course) {
		ListIterator<Student> itr = list.listIterator();
		while (itr.hasNext()) {
			Student st = itr.next();
			if (st.getCourse() == course) {
				System.out.println(st);
			}
		}
	}

	public static List<Student> sortStudents(List<Student> list) {
		List<Student> itr = new ArrayList<>(list);
		Comparator<Student> comp = new Comparator<Student>() {

			@Override
			public int compare(Student o1, Student o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};

		Collections.sort(itr, comp);
		return itr;
	}

	public static Map<String, Integer> createMap(String path) throws Exception {
		Map<String, Integer> map = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			String s;
			while ((s = reader.readLine()) != null)
				sb.append(s).append("\n");
		} catch (FileNotFoundException e) {
			throw new Exception("Can't read the file");
		}
		String utility = sb.toString();
		utility = utility.replaceAll("[\\.|\\,|\\?|\\!\\;\\:]", "");
		utility = utility.replaceAll("[\\n]", " ");
		String[] utility_array = utility.split("\\s{1,}");
		for (int i = 0; i < utility_array.length; i++) {
			if (!map.containsKey(utility_array[i])) {
				map.put(utility_array[i], 1);
			}
			if (map.containsKey(utility_array[i])) {
				map.put(utility_array[i], map.get(utility_array[i]) + 1);
			}

		}

		return map;
	}

	public static void sortMap(Enum order, Map<String, Integer> map) {
		if (order.equals(Order.BY_ALPABIT_TO_END)) {
			Comparator<String> comp = new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);
				}
			};

		}

	}
}
