package ua.org.oracle_academy.practice4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class Demo {
	public static Demo demo = new Demo();

	public static void main(String[] args) throws Exception {

		List<Student> listStudents = new ArrayList<>();
		listStudents.add(new Student("Vasya", "Petrov", 4));
		listStudents.add(new Student("Petya", "Stepanov", 3));
		listStudents.add(new Student("Dima", "Sidorov", 4));
		listStudents.add(new Student("Anya", "Ivanova", 3));
		ListIterator itr = listStudents.listIterator();
		System.out.println(itr.next());
		System.out.println(itr.next());
		itr.remove();
		System.out.println(itr.next());
		System.out.println(listStudents.size());

		System.out.println(listStudents.toString());

		System.out.println(StudentUtils.createMap("Romeo.txt"));
	}

}
