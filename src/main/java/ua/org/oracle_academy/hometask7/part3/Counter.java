package ua.org.oracle_academy.hometask7.part3;

public class Counter {
	private int count1;
	private int count2;

		public int getCount1() {
		return count1;
	}

	public void setCount1(int count1) {
		this.count1 = count1;
	}

	public int getCount2() {
		return count2;
	}

	public void setCount2(int count2) {
		this.count2 = count2;
	}

}
