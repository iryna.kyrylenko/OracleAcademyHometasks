package ua.org.oracle_academy.hometask7.part3;

public class MyThread extends Thread {
	private Counter c;

	public MyThread(Counter c) {
		this.c = c;
	}

	public synchronized boolean compareCounts(int i, int z) {
		if (i == z) {
			return true;
		}
		return false;
	}

	public synchronized int incrementCount(int i) {
		i++;
		return i;
	}

	@Override
	public void run() {
		int i = 0;

		while (i < 5) {
			try {
				System.out.println("Counter1 = " + c.getCount1() + ", Counter 2 = " + c.getCount2());
				System.out.println(compareCounts(c.getCount1(), c.getCount2()));

				c.setCount1(incrementCount(c.getCount1()));
				sleep(10);
				c.setCount2(incrementCount(c.getCount2()));
				i++;
			} catch (InterruptedException e) {
				System.out.println("exception");
			}
		}

	}
}
