package ua.org.oracle_academy.hometask7.part3;

public class Demo {

	public static void main(String[] args) {
		Counter c = new Counter();
		Thread t1 = new MyThread(c);
		Thread t2 = new MyThread(c);
		t1.start();
		t2.start();

	}

}
