package ua.org.oracle_academy.hometask7.part1;

import java.time.LocalDateTime;

public class MyRunnable implements Runnable {

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				return;
			}
			System.out.println("RUNNABLE" + LocalDateTime.now());
		}

	}

}
