package ua.org.oracle_academy.hometask7.part1;

import java.util.Scanner;


public class Demo {

	public static void main(String[] args) throws InterruptedException {
		MyThread thread1 = new MyThread();
		thread1.start();		
		Thread thread2 = new Thread(new MyRunnable());
		thread2.start();
		Scanner sc = new Scanner(System.in);
		while(true) {
			if (sc.hasNextLine()) {
				thread1.interrupt();
				thread2.interrupt();
				break;
			}
		}
		
	}

}
