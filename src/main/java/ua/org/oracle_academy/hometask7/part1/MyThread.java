package ua.org.oracle_academy.hometask7.part1;

import java.time.LocalDateTime;

public class MyThread extends Thread {

	@Override
	public void run() {
		while (true) {
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				return;
			}
			System.out.println("THREAD" + LocalDateTime.now());

		}
	}

}
