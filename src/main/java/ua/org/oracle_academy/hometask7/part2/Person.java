package ua.org.oracle_academy.hometask7.part2;

public class Person extends Thread {
	
	private String personName;
	private Phone phone;
	private Person friend;

	public Person(String personName, Phone phone) {
		this.personName = personName;
		this.phone = phone;
	}

	@Override
	public void run() {
		getPhone().makeCall(friend, this);
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public Person getFriend() {
		return friend;
	}

	public void setFriend(Person friend) {
		this.friend = friend;
	}

	@Override
	public String toString() {
		return "Person [personName=" + personName + ", phone=" + phone + ", friend=" + friend + "]";
	}

}
