package ua.org.oracle_academy.hometask7.part2;

public class Phone {

	public synchronized void makeCall(Person to, Person from) {
		to.getPhone().receiveCall(from);
	}
	public synchronized void receiveCall(Person from){
		System.out.println("Incoming call...");
	}
}
