package ua.org.oracle_academy.hometask7.part2;

public class Demo {

	public static void main(String[] args) throws InterruptedException {

		Person p1 = new Person("Sveta", new Phone());
		Person p2 = new Person("Misha", new Phone());
		p1.setFriend(p2);
		p2.setFriend(p1);
		p1.start();
		p2.start();
	}

}
