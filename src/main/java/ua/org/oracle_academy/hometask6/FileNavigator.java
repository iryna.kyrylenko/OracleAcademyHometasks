package ua.org.oracle_academy.hometask6;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class FileNavigator {

	public static final String CREATE_FILE_COMMAND = "create file";
	public static final String CREATE_FILE_DIRECTORY = "create directory";
	public static final String RENAME_FILE_COMMAND = "rename file";
	public static final String DELETE_FILE_COMMAND = "delete file";
	public static final String VIEW_FILES_COMMAND = "view files in directory";
	public static final String EXIT_PROGRAM = "exit";

	private Map<String, Command> commands;

	public FileNavigator() {
		initCommands();
	}

	public static void main(String[] args) throws IOException {
		FileNavigator navigator = new FileNavigator();
		System.out.println("Welcome to FileNavigator!");
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			while (true) {
				String command = selectCommand(reader, navigator.commands.keySet().toArray());
				try {
					boolean result = navigator.executeCommand(command, reader);
					System.out.println("Result -> " + (result ? "successful" : "unsuccessful"));
				} catch (IllegalArgumentException e) {
					System.out.println(e.getMessage());
					System.out.println("Please try again.");
				}
			}
		}

	}

	private boolean executeCommand(String command, BufferedReader reader) throws IOException {
		return commands.get(command).execute(reader);
	}

	private static String selectCommand(BufferedReader reader, Object[] commands) {
		while (true) {
			try {
				System.out.println("Please select command:");
				printCommands(commands);
				String input = reader.readLine().trim();
				if (commands[Integer.parseInt(input) - 1] != null) {
					return (String) commands[Integer.parseInt(input) - 1];
				}
			} catch (ArrayIndexOutOfBoundsException | IOException | NumberFormatException e) {
				System.out.println("Invalid input! Please try again.");
			}
		}
	}

	private void initCommands() {
		commands = new LinkedHashMap<String, Command>();
		commands.put(CREATE_FILE_COMMAND, new Command() {
			public boolean execute(BufferedReader reader) throws IOException {
				System.out.println("Please enter name of new file:");
				String[] args = receiveArguments(reader);
				validateFileNotExists(args[0]);
				return new File(args[0]).createNewFile();
			}
		});
		commands.put(CREATE_FILE_DIRECTORY, new Command() {
			public boolean execute(BufferedReader reader) throws IOException {
				System.out.println("Please enter name of new directory:");
				String[] args = receiveArguments(reader);
				validateFileNotExists(args[0]);
				return new File(args[0]).mkdirs();
			}
		});
		commands.put(RENAME_FILE_COMMAND, new Command() {
			public boolean execute(BufferedReader reader) throws IOException {
				System.out.println("Please enter name of file to rename:");
				String initialName = receiveArguments(reader)[0];
				validateFileExists(initialName);
				System.out.println("Please target name of file:");
				String targetName = receiveArguments(reader)[0];
				return new File(initialName).renameTo(new File(targetName));
			}
		});
		commands.put(DELETE_FILE_COMMAND, new Command() {
			public boolean execute(BufferedReader reader) throws IOException {
				System.out.println("Please enter name of file to delete:");
				String[] args = receiveArguments(reader);
				validateFileExists(args[0]);
				return new File(args[0]).delete();
			}
		});
		commands.put(VIEW_FILES_COMMAND, new Command() {
			public boolean execute(BufferedReader reader) throws IOException {
				File dir = new File("").getAbsoluteFile();
				for (File file : dir.listFiles()) {
					System.out.println(file.getName());
				}
				return true;
			}
		});
		commands.put(EXIT_PROGRAM, new Command() {
			public boolean execute(BufferedReader reader) throws IOException {
				System.exit(0);
				return false;
			}
		});
	}

	private void validateFileExists(String arg) {
		if (!new File(arg).exists()) {
			throw new IllegalArgumentException("File not exists!");
		}
	}

	private void validateFileNotExists(String arg) {
		if (new File(arg).exists()) {
			throw new IllegalArgumentException("File already exists!");
		}
	}

	private static void printCommands(Object[] commands) {
		int i = 0;
		for (Object command : commands) {
			System.out.println(++i + " : " + command);
		}
	}

	private abstract class Command {
		public abstract boolean execute(BufferedReader reader) throws IOException;

		public String[] receiveArguments(BufferedReader reader) throws IOException {
			return reader.readLine().split(" ");
		}
	}

}
