package ua.org.oracle_academy.hometask6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BookManager {

	private static final String BOOK_REGEXP = "\\p{L}+;\\p{L}+;\\d+";
	private static final String DELIMETER = ";";

	public static void main(String[] args) throws IOException {
		BookManager manager = new BookManager();
		Book book = new Book();
		book.setTitle("title");
		book.setAuthor("author");
		book.setYear(1999);
		File file = new File("books.txt");
		manager.writeBook(file, book);
		System.out.println(manager.readBooks(file).get(0).equals(book));
		file.delete();
	}

	public List<Book> readBooks(File file) throws IOException {
		List<Book> books = new ArrayList<Book>();
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			for (String line; (line = reader.readLine()) != null;) {
				if (isValidLine(line)) {
					books.add(parseBook(line));
				} else {
					continue;
				}
			}
		}
		return books;
	}

	public void writeBook(File file, Book... books) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
			for (Book book : books) {
				writer.newLine();
				writer.write(book.toString());
			}
		}
	}

	private boolean isValidLine(String line) {
		return line.matches(BOOK_REGEXP);
	}

	private Book parseBook(String line) {
		String[] elements = line.split(DELIMETER);
		Book book = new Book();
		book.setTitle(elements[0]);
		book.setAuthor(elements[1]);
		book.setYear(Integer.parseInt(elements[2]));
		return book;
	}

}
