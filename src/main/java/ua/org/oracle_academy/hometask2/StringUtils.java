package ua.org.oracle_academy.hometask2;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	/*
	 * This method reverses String by using StringBuilder
	 */
	public static String reverseString(String str) {
		StringBuilder sb = new StringBuilder(str);
		sb = sb.reverse();
		return sb.toString();
	}

	/*
	 * This method returns true/false. Works as you can see with Russian
	 * language only
	 */
	public static boolean isPalindromeString(String str) {
		String s1 = str.toLowerCase().replaceAll("[^�-�]", "");
		String s2 = reverseString(str).toLowerCase().replaceAll("[^�-�]", "");
		boolean result = s1.equalsIgnoreCase(s2);
		return result;
	}

	/*
	 * This method checks string, if its length()<=10, it adds 'o' until length
	 * == 12. Otherwise it returns cutted string(6 symbols).
	 */
	public static String check10(String str) {
		int n = 12 - str.length();
		if (str.length() <= 10) {
			for (int i = 0; i < n; i++) {
				str = str.concat("o");
			}
			return str;
		}
		return str.substring(0, 7);
	}

	/*
	 * This method changes last word to first & first word to last.
	 */
	public static String changeWordsPlace(String str) {
		String firstword = "";
		String lastword = "";
		Matcher m1 = Pattern.compile("\\w[a-z]+").matcher(str);
		Matcher m2 = Pattern.compile("\\w+$").matcher(str);
		if (m1.find())
			firstword = m1.group();

		if (m2.find()) {
			lastword = m2.group();
		}
		str = str.replaceFirst(lastword, firstword);
		str = str.replaceFirst(firstword, lastword);
		return str;
	}

	/*
	 * This method changes last word to first & first to last one in each
	 * sentence. We also use previous method changeWordsPlace() for that
	 */
	public static String changeWordsPlaceInSentences(String str) {
		String[] array = str.split("\\.");
		String result = "";
		for (int i = 0; i < array.length; i++) {
			array[i] = changeWordsPlace(array[i]) + ".";
			result = result + array[i];
		}

		return result;
	}

	/*
	 * This method return true if string consist of lower case 'a', 'b', 'c' symbols only
	 */
	public static boolean isAbc(String str) {
		int count = 0;
		Pattern p = Pattern.compile("a|b|c");
		Matcher m = p.matcher(str);
		while (m.find()) {
			count++;
		}
		return str.length() == count;
	}

	/*
	 * This method checks is the entering string a date, format "XX.XX.XXXX"
	 */
	public static boolean isDate(String str) { // �������� �� ������ �����
		Pattern p = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}+");
		Matcher m = p.matcher(str);
		return m.matches();
	}

	/*
	 * This method checks is the entering string a email
	 */
	public static boolean isEmail(String str) {
		Pattern p = Pattern.compile("\\w+[@]+[a-z]{1,6}+\\.?[a-z]{2,3}");
		Matcher m = p.matcher(str);
		return m.matches();
	}

	/*
	 * This method finds all phone numbers in text (+�(���)���-��-��) and forms
	 * filling ArrayList
	 */
	public static ArrayList<String> searchPhone(String str) {
		ArrayList<String> list = new ArrayList<String>();
		Pattern p = Pattern.compile("[+][0-9][(][0-9]{3}[)][0-9]{3}[-][0-9]{2}[-][0-9]{2}");
		Matcher m = p.matcher(str);
		while (m.find()) {
			list.add(m.group());
		}
		return list;
	}

}
