package ua.org.oracle_academy.hometask2;

import static java.lang.String.format;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Class is still improving to correct working. Now it's not right for 100%
 */
public class MarkdownParser {
	/*
	 * Variables that are regular expressions, must be constants(for each object
	 * of MarkdownParser the same)
	 */
	private static final char HEADER_SYMBOL = '#';

	private static final String HEADER_REGEXP = "(?m)^(#+)(.*)";
	private static final String PARAGRAPH_REGEXP = "(?m)^(?!<h[1-6]>)(.*)";
	private static final String STRONG_REGEXP = "\\*{3}(.*?)\\*{3}";
	private static final String EMPHASI_REGEXP = "\\*{2}(.*?)\\*{2}";
	private static final String LINK_REGEXP = "\\[(.+)\\]\\((.+)\\)";

	private static final String HEADER_TAG = "<h%d>%s</h%d>";
	private static final String PARAGRAPH_TAG = "<p>%s</p>";
	private static final String STRONG_TAG = "<strong>%s</strong>";
	private static final String EMPHASI_TAG = "<em>%s</em>";
	private static final String LINK_TAG = "<a href=\"%s\">%s</a>";

	/*
	 * Main class's method parse() implemented by using called methods
	 */
	public String parse(String str) {
		str = processHeaders(str);
		str = processParagraphs(str);
		str = processStrong(str);
		str = processEmphasi(str);
		str = processLinks(str);
		return str;
	}

	/*
	 * This utility method helps to create matcher for regex on easy way
	 */
	private Matcher getMatcher(String pattern, String str) {
		return Pattern.compile(pattern).matcher(str);
	}

	private String processHeaders(String str) {
		Matcher m = getMatcher(HEADER_REGEXP, str);
		while (m.find()) {
			int symbols = countSymbolsInRow(HEADER_SYMBOL, m.group());
			str = str.replace(m.group(), format(HEADER_TAG, symbols, m.group(2), symbols));
		}
		return str;
	}

	private String processParagraphs(String str) {
		Matcher m = getMatcher(PARAGRAPH_REGEXP, str);
		while (m.find()) {
			str = str.replace(m.group(), format(PARAGRAPH_TAG, m.group()));
		}
		return str;
	}

	private String processStrong(String str) {
		Matcher m = getMatcher(STRONG_REGEXP, str);
		while (m.find()) {
			str = str.replace(m.group(), format(STRONG_TAG, m.group(1)));
		}
		return str;
	}

	private String processEmphasi(String str) {
		Matcher m = getMatcher(EMPHASI_REGEXP, str);
		while (m.find()) {
			str = str.replace(m.group(), format(EMPHASI_TAG, m.group(1)));
		}
		return str;
	}

	private String processLinks(String str) {
		Matcher m = getMatcher(LINK_REGEXP, str);
		while (m.find()) {
			str = str.replace(m.group(), format(LINK_TAG, m.group(2), m.group(1)));
		}
		return str;
	}

	private int countSymbolsInRow(char symbol, String str) {
		int amount = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == symbol) {
				amount++;
			} else {
				break;
			}
		}
		return amount;
	}

}
