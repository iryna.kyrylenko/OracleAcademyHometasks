package ua.org.oracle_academy.hometask1;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

public class Date {

	private Month month;
	private Year year;
	private Day day;

	public Date(int day, int month, int year) {
		this.month = new Month(month);
		this.year = new Year(year);
		this.day = new Day(day);
	}

	/**
	 * The method returns the day of the year by current date
	 */
	public static int getDayOfYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(date.year.number, date.month.number - 1, date.day.number);
		int result = calendar.get(Calendar.DAY_OF_YEAR);
		return result;
	}

	/**
	 * The method returns the day of the week(an object of Enum) by entering
	 * date
	 */
	public static DayOfWeek getDayOfWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(date.year.number, date.month.number, date.day.number);
		DayOfWeek day_of_week = DayOfWeek.valueOf(calendar.get(Calendar.DAY_OF_WEEK) - 1);
		return day_of_week;
	}

	/**
	 * The method returns a difference (the number of days between two dates)
	 */
	public static int daysBetween(Date date1, Date date2) {
		Calendar d1 = Calendar.getInstance();
		Calendar d2 = Calendar.getInstance();
		d1.set(date1.year.number, date1.month.number, date1.day.number);
		d2.set(date2.year.number, date2.month.number, date2.day.number);
		return Math.abs((int) ((d2.getTimeInMillis() - d1.getTimeInMillis()) / 1000 / 3600 / 24));
	}

	/**
	 * The method returns an amount of days in this month
	 */
	public static int daysInMonth(Date date) {
		switch (date.month.number) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		case 2:
			return date.year.leapYear ? 29 : 28;
		}
		return -1;

	}

	/**
	 * In Year constructor after creating object is it Leap checks
	 */
	public class Year {
		private int number;
		public boolean leapYear;

		public Year(int number) {
			if (number % 4 == 0 && number % 100 != 0 || number % 400 == 0) {
				this.leapYear = true;
			}
			this.number = number;
		}

		public int getNumber() {
			return number;
		}

		@Override
		public String toString() {
			return "Year [number=" + number + ", isLeapYear=" + leapYear + "]";
		}

		public boolean isLeapYear() {
			return leapYear;
		}
	}

	/**
	 * In Month constructor we use class DateFormatSymbols, that assigns month
	 * name by indexing
	 */
	public class Month {
		private int number;
		private String name;

		public Month(int number) {
			this.number = number;
			this.name = new DateFormatSymbols(Locale.ENGLISH).getMonths()[number - 1];
		}

		public int getNumber() {
			return number;
		}

		@Override
		public String toString() {
			return "Month [number=" + number + ", name=" + name + "]";
		}
	}

	public class Day {
		private int number;

		public Day(int number) {
			this.number = number;
		}

		public int getNumber() {
			return number;
		}

		@Override
		public String toString() {
			return "Day [number=" + number + "]";
		}

	}

	public enum DayOfWeek {
		SUNDAY(0), MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6);
		private final int index;

		private DayOfWeek(int index) {
			this.index = index;
		}

		/**
		 * The method return constant object by its index
		 */
		public static DayOfWeek valueOf(int index) {
			for (DayOfWeek day : values()) {
				if (day.index == index) {
					return day;
				}
			}
			throw new IllegalArgumentException("No such day of week!");
		}
	}

	public Day getDay() {
		return this.day;
	}

	public Month getMonth() {
		return this.month;
	}

	public Year getYear() {
		return this.year;
	}

	@Override
	public String toString() {
		return "Date [month=" + month + ", year=" + year + ", day=" + day + "]";
	}
}
