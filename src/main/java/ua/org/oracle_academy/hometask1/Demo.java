package ua.org.oracle_academy.hometask1;

/**
 * In class Demo demonstrated the creation of an anonymous classes objects. We
 * can say that by that way new added attributes extended the class Car
 * (Hometask. Part 2)
 */
public class Demo {
	public static void main(String[] args) {

		Car velocycle = new Car() {
			String name = "Velocycle";
			String model = "X902";
			int year = 2007;

			@Override
			public String toString() {
				return "Velocycle [name=" + name + ", model=" + model + ", year=" + year + "]";
			}

			@Override
			public boolean equals(Object obj) {
				return super.equals(obj);
			}

		};
		Car demo_car = new Car() {
			String name = "Demo car";
			int wheels = 3;
			boolean result = false;

			@Override
			public String toString() {
				return "Test object [name=" + name + ", number of wheels=" + wheels + ", testing result=" + result
						+ "]";
			}

			@Override
			public boolean equals(Object obj) {
				return super.equals(obj);
			}

		};
		System.out.println(velocycle.toString());
		System.out.println(demo_car.toString());
		System.out.println(velocycle.equals(demo_car));

	}

}
