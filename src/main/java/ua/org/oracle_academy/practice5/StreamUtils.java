package ua.org.oracle_academy.practice5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class StreamUtils {

	public static void fillWithRandomInts(String path, int range, int amount) {
		Random random = new Random();
		int counter_range = 0;
		int random_int = 0;

		try (BufferedWriter bf = new BufferedWriter(new FileWriter(path))) {
			while (counter_range < amount) {
				counter_range++;
				random_int = random.nextInt(range);

				bf.write(String.valueOf(random_int).concat("\n"));
			}
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static void sortIntsFromFile(String path) {
		List<Integer> list = new ArrayList<>();
		String intLine = "";

		try (BufferedReader bf = new BufferedReader(new FileReader(path))) {
			while ((intLine = bf.readLine()) != null) {
				list.add(Integer.parseInt(intLine));
			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Collections.sort(list);
		
		ListIterator<Integer> itr = list.listIterator();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
			int counter = 0;
			while (counter < list.size()) {
				counter++;
				bw.write(String.valueOf(itr.next()).concat("\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
	
public static void averageMore90(String path){
	HashMap<String, List<Integer>> map = new HashMap<>();
	List<Integer> list = new ArrayList<>();
	
	String studentLine = "";
	
	try (BufferedReader bf = new BufferedReader(new FileReader(path))) {
		while((studentLine=bf.readLine())!=null){
			if(map.get(name) == null){
			map.put(name, new ArrayList(+));  		
			
		}
		 catch (FileNotFoundException e) {
	
		e.printStackTrace();
	} catch (IOException e) {
	
		e.printStackTrace();
	}
	
	
}