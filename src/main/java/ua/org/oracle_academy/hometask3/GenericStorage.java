package ua.org.oracle_academy.hometask3;

import java.util.Arrays;

public class GenericStorage<T> {
	private T[] generic_storage;
	private static final int DEFAULT_SIZE = 10;
	private int next_index = 0;

	public GenericStorage() {
		this(DEFAULT_SIZE);
	}

	public GenericStorage(int size) {
		generic_storage = (T[]) new Object[size];
	}

	/*
	 * This method adds new elements in array
	 */
	public void add(T object) {
		generic_storage[next_index] = object;
		next_index++;
	}

	/*
	 * This method returns an element of array by entering index
	 */
	public T get(int index) {
		if (index > generic_storage.length - 1 || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		return generic_storage[index];

	}

	/*
	 * This method returns an array with added elements only
	 *  P.S. ��� � �������,
	 * � ������� �� ������� �������, ������� ������ ������ ��� ������ ������
	 * size ���� � null-���, ������ ����� ������ ������� ������ � ������������
	 * ��������. ���� ��� - ��������, ��� ��� ������ ������ ����� ��������
	 * �������� return generic_storage.
	 */
	public T[] getAll() {
		int stop = 0;
		for (int i = 0; i < generic_storage.length; i++) {
			if (generic_storage[i] == null) {
				stop = i;
				break;
			}
		}
		T[] utility_storage = (T[]) new Object[stop];
		System.arraycopy(generic_storage, 0, utility_storage, 0, stop);
		return utility_storage;

	}

	/*
	 * This method replaced an element that placed on entering index by another
	 * one and returns true. If there wasn't an any element on that index,
	 * method returns false
	 */
	public boolean update(int index, T object) {
		if (index > generic_storage.length - 1 || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		if (generic_storage[index] == null) {
			return false;
		} else {
			generic_storage[index] = object;
		}
		return true;
	}

	/*
	 * This method removes an element that placed on entering index from array
	 */
	public void delete(int index) {
		if (index > generic_storage.length - 1 || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		for (int i = index; i < generic_storage.length - 1; i++) {
			generic_storage[i] = generic_storage[i + 1];
			if (i == generic_storage.length - 2) {
				generic_storage[i + 1] = null;
			}

		}

	}

	/*
	 * This method returns true if there is an entering argument in array and if
	 * it was successfully deleted
	 */
	public boolean delete(T object) {
		for (int i = 0; i < generic_storage.length; i++) {
			if (generic_storage[i] == object) {
				for (int j = i; j < generic_storage.length - 1; j++) {
					generic_storage[j] = generic_storage[j + 1];
				}
				generic_storage[generic_storage.length - 1] = null;
				return true;
			}

		}
		return false;
	}
/*
 * This method returns a filled array
 */
	public int amountOfElements(){
		return getAll().length;
	}
	
	/*
	 * This method returns an array, which size = array.length
	 */
	public T[] getGeneric_storage() {
		return generic_storage;
	}

	@Override
	public String toString() {
		return "GenericStorage [generic_storage=" + Arrays.toString(generic_storage) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(generic_storage);
		result = prime * result + next_index;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenericStorage other = (GenericStorage) obj;
		if (!Arrays.equals(generic_storage, other.generic_storage))
			return false;
		if (next_index != other.next_index)
			return false;
		return true;
	}
}
