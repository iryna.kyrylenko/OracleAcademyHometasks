package ua.org.oracle_academy.practice1;

import org.junit.Assert;
import org.junit.*;
import ua.org.oracle_academy.practice1.ArraySum;

public class ArraySumTest {

	private int[] ints;

	@Before
	public void createArraySum() {
		ints = new int[] { 1, 2, 3, 6, 7 };
	}

	@Test
	public void testSum() {
		int expected = 0;
		for (int anInt : ints) {
			expected += anInt;
		}

		int actual = new ArraySum(ints).sum();
		Assert.assertEquals(expected, actual);

	}

	@Test
	public void testStaticSum() {

		int expected = 0;
		for (int anInt : ints) {
			expected += anInt;
		}
		int actual = ArraySum.sum(ints);
		Assert.assertEquals("Expected :" + expected + ", but was : " + actual, expected, actual);
	}

	@Test(expected = NullPointerException.class)
	public void testNullSum() {
		ArraySum.sum(null);
	}

}
