package ua.org.oracle_academy.hometask2;

import java.util.ArrayList;

import org.junit.*;

public class StringUtilsTest {

	@Test
	public void reverseStringTest() {
		String example = "Change world";
		String expected = new StringBuilder(example).reverse().toString();
		String actual = StringUtils.reverseString(example);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void isPalindromeStringTest() {
		String example = "� ���� ����� �� ���� �����";
		boolean expected = true;
		boolean actual = StringUtils.isPalindromeString(example);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void check0Test1() {
		String example = "Example";
		String expected = "Exampleooooo";
		String actual = StringUtils.check10(example);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void check0Test2() {
		String example = "1234567 example";
		String expected = "1234567";
		String actual = StringUtils.check10(example);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void changeWordsPlaceTest1() {
		String example = "It was such a beautiful day";
		String expected = "day was such a beautiful It";
		String actual = StringUtils.changeWordsPlace(example);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void changeWordsPlaceTest2() {
		String example = "It was such a beautiful day";
		String expected = "It was such a beautiful It";
		String actual = StringUtils.changeWordsPlace(example);
		Assert.assertNotEquals(expected, actual);
	}

	@Test
	public void changeWordsPlaceTestInSentences1() {
		String example = "It was such a beautiful day.What a nice surprise.";
		String expected = "day was such a beautiful It.surprise a nice What.";
		String actual = StringUtils.changeWordsPlaceInSentences(example);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void isAbcTest() {
		String example = "abababaaaac";
		boolean expected = true;
		boolean actual = StringUtils.isAbc(example);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void isDateTest1() {
		String example = "29.05.2017";
		boolean expected = true;
		boolean actual = StringUtils.isDate(example);
		Assert.assertEquals(expected, actual);
	}

	public void isDateTest2() {
		String example = "29.0505.2017";
		boolean expected = true;
		boolean actual = StringUtils.isDate(example);
		Assert.assertNotEquals(expected, actual);
	}

	@Test
	public void isEmailTest1() {
		String example = "vasya@yandex.ru";
		boolean expected = true;
		boolean actual = StringUtils.isEmail(example);
		Assert.assertEquals(expected, actual);

	}

	@Test
	public void isEmailTest2() {
		String examle = "vasya451@i.com";
		boolean expected = true;
		boolean actual = StringUtils.isEmail(examle);
		Assert.assertEquals(expected, actual);

	}

	@Test
	public void isEmailTest3() {
		String example = "vasyayandex.ru";
		boolean expected = true;
		boolean actual = StringUtils.isEmail(example);
		Assert.assertNotEquals(expected, actual);
	}

	@Test
	public void searchPhoneTest() {
		String example = "Hello, that's my number +3(000)288-55-16. "
				+ "You can call me during 2.00PM - 5.00PM. "
				+ "Also you can call on business phone, write it +5(999)123-32-00, bye";
		ArrayList <String> expected = new ArrayList<String>();
		expected.add("+3(000)288-55-16");
		expected.add("+5(999)123-32-00");
		ArrayList<String> actual = StringUtils.searchPhone(example);
		Assert.assertEquals(expected, actual);
	}
}
